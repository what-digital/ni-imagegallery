********
Settings
********

Settings may be overridden in your settings. Don't forget to add the prefix **NI_IMAGEGALLERY_**

**SORTED_M2M_FORM_PATH**
default: *'ni_imagegallery.forms.SortedMultipleChoiceField'*
Entry point for a custom form.

**IMAGE_FILE_TYPES**
default *['png', 'jpg', 'jpeg']*

**THUMBNAIL_ALIASES**
default::
{
    'admin-small': {'size': (75, 75)},
    'thumbnail': {'size': (150, 150)},
    'full': {'size': (800, 0)},
}
