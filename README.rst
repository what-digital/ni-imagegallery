#################################################################################
NI Image Gallery
#################################################################################

This app lets you create galleries from files uploaded with the filer.

Quickstart
==========

Add 'ni_imagegallery' to the ``ÌNSTALLED_APPS```, extend from ```Gallery``` in your models to use it.
Include 'ni_imagegallery.urls' in your url-patterns.

Contributing
============

To contribute:

* Clone this project somewhere on your development machine.
* Add ``ni_imagegallery`` to the *INSTALLED_APPS* setting of a new or an existing Django project.
* Add the path of the folder containing this file onto the Python path.
* Optional: Add an additional *Content Root* in PyCharm pointing to the path of the folder containing this file.
* Start developing.

More information
================

This app heavily depends on django-sortedm2m (python) with some monkey patching for configurable elements and
jqueryFileTree (JS). Please have a look at the documentation for further information about the usage of this app.

License
=======

This is proprietary code of Notch Interactive GmbH.
