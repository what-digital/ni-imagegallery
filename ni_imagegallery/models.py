# -*- coding: utf-8 -*-
import operator

from django.db import models
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from easy_thumbnails.files import get_thumbnailer
from filer.models import File
from model_utils.models import TimeStampedModel
from sortedm2m.fields import SortedManyToManyField

from .conf import settings
from .utils import extended_filer_file_unicode


# Monkey patch SortedManyToManyField, so we can use the form as defined in the settings.
def formfield(self, **kwargs):
    defaults = {}
    if self.sorted:
        defaults['form_class'] = settings.NI_IMAGEGALLERY_SORTED_M2M_FORM
    defaults.update(kwargs)
    return super(SortedManyToManyField, self).formfield(**defaults)
SortedManyToManyField.formfield = formfield


class Gallery(TimeStampedModel):
    """
    Minimal required models for a gallery. Customize this model with the setting in conf.py.
    """
    images = SortedManyToManyField(
        File,
        limit_choices_to=reduce(
            operator.or_,
            [Q(original_filename__iendswith=extension) for extension in settings.NI_IMAGEGALLERY_IMAGE_FILE_TYPES]
        ),
        **settings.NI_IMAGEGALLERY_IMAGE_FIELD_PARAMS
    )  # limit queryset to configured file-types; __iendswith can't be combined with __in, so use a Q-object.

    @property
    def nb_elements(self):
        return self.images.all().count()

    def thumbnails_list(self, thumbnail='thumbnail', full='full'):
        # Returns a list of dicts with 'thumbnail' and 'full' images.
        return [
            {
                'thumbnail': get_thumbnailer(image.file).
                    get_thumbnail(settings.NI_IMAGEGALLERY_THUMBNAIL_ALIASES[thumbnail]).url,
                'large': get_thumbnailer(image.file).
                    get_thumbnail(settings.NI_IMAGEGALLERY_THUMBNAIL_ALIASES[full]).url
            } for image in self.images.all()
        ]

    class Meta:
        verbose_name_plural = _(u"Galleries")
        abstract = True
