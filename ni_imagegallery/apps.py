from django.apps import AppConfig


class NIImageGalleryConfig(AppConfig):
    name = 'ni_imagegallery'
    verbose_name = "NI Image Gallery"
