# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.template.loader import render_to_string

from easy_thumbnails.files import get_thumbnailer
from filer.models import File

from .conf import settings


def extended_filer_file_unicode(obj):

    if not isinstance(obj, File):
        try:
            obj = File.objects.get(id=obj)
        except File.DoesNotExist:
            return''

    return render_to_string('ni_imagegallery/includes/order-entry-content.html', {
        'filename': obj.name if obj.name else obj.original_filename,
        'file_id': obj.id,
        'media_url': get_thumbnailer(obj.file).get_thumbnail(settings.NI_IMAGEGALLERY_THUMBNAIL_ALIASES['admin-small']).url,
        'edit_url': reverse('admin:filer_file_change', args=(obj.id,)),
        'edit': _(u"edit"),
    })
