from django.conf.urls import patterns, url
from .views import FolderTreeView

urlpatterns = patterns(
    '',
    url(r'^$', FolderTreeView.as_view(), name='js-folder-tree-connect'),
)
