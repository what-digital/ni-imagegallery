# -*- coding: utf-8 -*-
import json
import operator

from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponse, Http404
from django.template.loader import render_to_string
from django.views.generic import View
from django.utils.html import escape

from easy_thumbnails.files import get_thumbnailer
from filer.models import File, Folder, FolderRoot
import itertools

from .conf import settings
from .utils import extended_filer_file_unicode


class FolderTreeView(View):
    """
    Returns the structure from django-filer for the JQuery Plugin.
    We can't just browse a dir in /media, as not all objects may be File-objects or some deleted objects may still live
    in the folders.
    """
    http_method_names = ['post']
    unfiled_files = 'unfiled'

    def post(self, request, *args, **kwargs):
        if not hasattr(request, 'user') or not request.user.is_staff:
            raise Http404()

        selected_dir = request.POST.get('folder', None) or None  # 'dir' may be empty string
        response = ['<ul class="jqueryFileTree" style="display: none;">']

        if not selected_dir:
            response.append(render_to_string('ni_imagegallery/includes/tree-folder.html',
                                             {'folder_id': self.unfiled_files, 'folder_name': _(u"Unfiled Files")}
            ))

        # Special case: files from the 'dummy'-folder (unfiled files).
        if selected_dir == self.unfiled_files:
            filer_files = itertools.chain(*[folder.files.all() for folder in FolderRoot().virtual_folders])

        else:
            folders = Folder.objects.filter(parent_id=selected_dir)
            for folder in folders:
                if hasattr(folder, 'has_read_permission') and not folder.has_read_permission(request):
                    continue

                response.append(render_to_string(
                    'ni_imagegallery/includes/tree-folder.html', {'folder_id': folder.id, 'folder_name': folder})
                )

            if selected_dir:
                filer_files = File.objects.filter(
                    reduce(
                        operator.or_,
                        [Q(original_filename__iendswith=extension) for extension in settings.NI_IMAGEGALLERY_IMAGE_FILE_TYPES]
                    ),
                    folder_id=selected_dir
                ).order_by('name', 'original_filename')
            else:  # No selected folder.
                filer_files = []

        # Get image files and markup for the "select-container".
        image_files = []
        for filer_file in filer_files:
            image_files.append(render_to_string('ni_imagegallery/includes/select-entry-content.html', {
                'media_url': get_thumbnailer(filer_file.file).get_thumbnail(settings.NI_IMAGEGALLERY_THUMBNAIL_ALIASES['admin-small']).url,
                'file_name': filer_file.name if filer_file.name else filer_file.original_filename,
                'file_id': filer_file.id,
                'order_markup': escape(render_to_string('ni_imagegallery/includes/select-entry-to-order-markup.html', {
                    'content': extended_filer_file_unicode(filer_file),
                    'file_id': filer_file.id,
                    'field_name': self.request.POST['field_name'].replace('id_', ''),
                }))
            }))

        response.append('</ul>')

        response_json = {
            'tree': ''.join(response),
            'files': ''.join(image_files)
        }
        return HttpResponse(json.dumps(response_json), content_type='json')
