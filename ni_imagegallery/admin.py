from django.contrib import admin
from django.db import models


class GalleryAdmin(admin.ModelAdmin):
    date_hierarchy = 'modified'
    search_fields = ['__unicode__']
    list_display = ['__unicode__', 'modified', 'number_of_images']

    def queryset(self, request):
        qs = super(GalleryAdmin, self).queryset(request)
        qs = qs.annotate(models.Count('images'))
        return qs

    def number_of_images(self, obj):
        return obj.nb_elements
    number_of_images.admin_order_field = 'images__count'
