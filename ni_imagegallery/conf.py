from django.conf import settings
from appconf import AppConf
from django.core.exceptions import ImproperlyConfigured
from django.utils.module_loading import import_by_path


class NIImageGalleryConf(AppConf):
    SORTED_M2M_FORM_PATH = 'ni_imagegallery.forms.SortedMultipleChoiceField'
    IS_ABSTRACT_MODEL = False
    IMAGE_FILE_TYPES = ['png', 'jpg', 'jpeg']
    THUMBNAIL_ALIASES = {
        'admin-small': {'size': (75, 75)},
        'thumbnail': {'size': (150, 150)},
        'full': {'size': (800, 0)},
    }
    IMAGE_FIELD_PARAMS = {}  # Pass additional arguments (e.g. 'blank': True} for field initialization

    class Meta:
        prefix = 'NI_IMAGEGALLERY'

    def configure(self):
        # Load classes for form and widget.
        for var in ['SORTED_M2M_FORM']:
            try:
                self.configured_data[var] = import_by_path(getattr(
                    settings, 'NI_IMAGEGALLERY_{}_PATH'.format(var), self.configured_data.get('{}_PATH'.format(var))
                ))
            except AttributeError:
                raise AttributeError("The class {0} could not be loaded from the string.".format(var))
            except ImproperlyConfigured:
                raise  # raise default ImportError
        return self.configured_data
