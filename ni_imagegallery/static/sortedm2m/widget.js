if (jQuery === undefined) {
    jQuery = django.jQuery;
}

var imagegalleryHelpers = (function ($) {

	function prepareUl(ul) {
		ul.addClass('sortedm2m');
		var checkboxes = ul.find('input[type=checkbox]');
		var id = checkboxes.first().attr('id').match(/^(.*)_\d+$/)[1];
		var name = checkboxes.first().attr('name');
		checkboxes.removeAttr('name');
		if (!$('#' + id).length) {
			ul.before('<input type="hidden" id="' + id + '" name="' + name + '" />');
		}

		var recalculate_value = function () {
			var values = [];
			$('.order-number').each(function (index) {
				$(this).text(index + 1);
			});
			ul.find(':checked').each(function () {
				values.push($(this).val());
			});
			$('#' + id).val(values.join(','));
		};

		recalculate_value();
		ul.on('change', 'input[type=checkbox]', recalculate_value);
		ul.sortable({
			axis: 'y',
			//containment: 'parent',
			update: recalculate_value
		});
	}

	function iterateUl() {
		$('.sortedm2m').parents('ul').each(function () {
			prepareUl($(this));
		});
	}

	// Fade entries in selection container that are already selected
	function fadeSelected () {
		var imagesArray;
		var images = $('#id_images').val();
		if(images) {
			 imagesArray = images.replace(/,\s+/g, ',').split(',');
		}
		var selectionArray = [];
		var imagesInSelect = $('#select-container li').each(function () {
			selectionArray.push($(this).attr('data-file-id'));
		});

		for (var i = 0; i < selectionArray.length; i++) {
			if (imagesArray.indexOf(selectionArray[i]) > -1) {
				$('li.add-to-selection[data-file-id='+selectionArray[i]+']').addClass('disabled', 1000);
			}
			else {
				$('li.add-to-selection[data-file-id='+selectionArray[i]+']').removeClass('disabled', 1000);
			}
		}
	}


	return {
			iterateUl: iterateUl,
			fadeSelected: fadeSelected
	};

})(jQuery);