if (jQuery === undefined) {
    jQuery = django.jQuery;
}

(function ($) {

	$(document).ready(function() {

		// Add entry to order container on click
		$('#select-container').on('click', '.add-to-selection',  function (e){
			var entry = $(e.currentTarget).closest('li.entry');
            var entryMarkup = entry.attr('data-order-markup');
			var entryId = entry.attr('data-file-id');

            $id_images = $('#id_images');
            if((! $($id_images).length) || jQuery.inArray(entryId, $($id_images).val().replace(/,\s+/g, ',').split(',')) === -1) {
                $('ul.sortedm2m_list_container').append(entryMarkup);
            } else {
                var duplicated = $('.duplicated');
	            duplicated.css({opacity: '1'});
                duplicated.fadeIn(500);
                setTimeout(function() {
                    duplicated.fadeOut(1500);
                }, 2000);
            }
			setTimeout(function() {
                imagegalleryHelpers.iterateUl();
				imagegalleryHelpers.fadeSelected();
            }, 0);
		});

		$('.drag-to-order').on('mouseover', function (){
			$('.duplicated').fadeOut(300);
		});

		// Reload file tree when clicking on root folder
        $('.root').on('click', function() {
            $('#folder-container').html('').fileTree(
                { root: '', script: $(this).attr('data-url') },
                function (file) {
                    $('#select-container').html(file);
            });
        });

		// Show filter box
		$('#filter').on('click', function (){
			$('#filter').fadeOut(500);
			$('.selector-filter input').css('opacity','1');
			$('.selector-filter').fadeIn(1000);
		});

        // Select all images from current folder.
        $('#select-all').on('click', function (e) {
            e.preventDefault();
            $('#select-container').find('li.entry').filter(
            function(){
                return !$('input[value="' + $(this).attr('data-file-id') + '"]').length;
            }).each(function() {
                // remove the 'name'-attr on the checkbox, as this attr is used for the 'hidden'
                // list with sorted images.
                var markup = $($(this).attr('data-order-markup'));
//                markup.find('input[type=checkbox]').removeAttr('name');
                $('#order-container ul.sortedm2m_list_container').append(markup);
            });
            imagegalleryHelpers.iterateUl();
	        imagegalleryHelpers.fadeSelected();
        });

		// Remove all images from order container
		$('#remove-all').on('click', function (e){
			e.preventDefault();
			$('.sortedm2m_list_container').html('');
			$('#id_images').val('');
			imagegalleryHelpers.fadeSelected();
		});

		// Clean up after image removal
		$('#order-container').on('click', '.image_remove_link', function (e) {
			var $currentTarget = $(e.currentTarget);
			$currentTarget.closest('label').find('input').prop('checked', false);
			var currentEntry = $currentTarget.closest('li');
			currentEntry.remove();
			imagegalleryHelpers.iterateUl();
			imagegalleryHelpers.fadeSelected();
		});

		// Filter for select container
		$('.selector-filter input').each(function () {
            $(this).on('input', function() {
                var search = $(this).val().toLowerCase();
                var $el = $(this).closest('.selector-filter');
                var $container = $el.siblings('#select-container').find('ul').each(function() {

                    // Walk over each child list el and do name comparisons
                    $(this).children().each(function() {
                        var curr = $(this).find('span').text().toLowerCase();
                        if (curr.indexOf(search) === -1) {
                            $(this).css('display', 'none');
                        } else {
                            $(this).css('display', 'inherit');
                        }
                    });
                });
            });
        });

		function showFilerPopup(triggeringLink) {
		    var name = triggeringLink.id.replace(/^add_/, '');
		    name = id_to_windowname(name);
		    var href = triggeringLink.href;
		    if (href.indexOf('?') == -1) {
		        href += '?_popup=1';
		    } else {
		        href  += '&_popup=1';
		    }
		    var win = window.open(href, name, 'height=500,width=800,resizable=yes,scrollbars=yes');
		    win.focus();

			var timer = setInterval(checkPopup, 500);

			// Check if filer popup was closed again in order redraw fileTree
			function checkPopup() {
			    if (win.closed) {
				    $('#folder-container').fileTree(
	                    { root: '', script: $('#api_url').attr('data-url') },
	                        function (file) {
	                            $('#select-container').append(file);
	                    }
                    );
			        clearInterval(timer);
			    }
			}
		    return false;
		}

		// Open filer popup on "add new images" click
		$('#add_id_folder').on('click', function (e){
			e.preventDefault();
			showFilerPopup(this);
		});

		if (window.showAddAnotherPopup) {
            var django_dismissAddAnotherPopup = window.dismissAddAnotherPopup;
            window.dismissAddAnotherPopup = function (win, newId, newRepr) {
                // newId and newRepr are expected to have previously been escaped by
                // django.utils.html.escape.
                newId = html_unescape(newId);
                newRepr = html_unescape(newRepr);
                var name = windowname_to_id(win.name);
                var elem = $('#' + name);
                var sortedm2m = elem.siblings('ul.sortedm2m');
                if (sortedm2m.length == 0) {
                    // no sortedm2m widget, fall back to django's default
                    // behaviour
                    return django_dismissAddAnotherPopup.apply(this, arguments);
                }

                if (elem.val().length > 0) {
                    elem.val(elem.val() + ',');
                }
                elem.val(elem.val() + newId);

                var id_template = '';
                var maxid = 0;
                sortedm2m.find('li input').each(function () {
                    var match = this.id.match(/^(.+)_(\d+)$/);
                    id_template = match[1];
                    id = parseInt(match[2]);
                    if (id > maxid) maxid = id;
                });

                var id = id_template + '_' + (maxid + 1);
                var new_li = $('<li/>').append(
                    $('<label/>').attr('for', id).append(
                        $('<input class="sortedm2m" type="checkbox" checked="checked" />').attr('id', id).val(newId)
                    ).append($('<span/>').text(' ' + newRepr))
                );
                sortedm2m.append(new_li);
                win.close();
            };
        }
	});

	// Initialize proper layout of order container on start
	setTimeout(function () {
		imagegalleryHelpers.iterateUl();
		imagegalleryHelpers.fadeSelected();
	}, 100);

})(jQuery);
