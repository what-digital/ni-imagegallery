# -*- coding: utf-8 -*-
import sys
from itertools import chain

from django import forms
from django.template.loader import render_to_string
from django.utils.encoding import force_text
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe

from sortedm2m.forms import SortedCheckboxSelectMultiple

from .conf import settings
from .utils import extended_filer_file_unicode

if sys.version_info[0] < 3:
    iteritems = lambda d: iter(d.iteritems())
    string_types = basestring,
    str_ = unicode
else:
    iteritems = lambda d: iter(d.items())
    string_types = str,
    str_ = str


class NIGallerySortedCheckboxSelectMultiple(SortedCheckboxSelectMultiple):
    class Media:
        js = (
            settings.STATIC_URL + 'sortedm2m/widget.js',
            settings.STATIC_URL + 'sortedm2m/jquery-ui.js',
            settings.STATIC_URL + 'sortedm2m/jquery.easing.js',
            settings.STATIC_URL + 'sortedm2m/jqueryFileTree.js',
            settings.STATIC_URL + 'sortedm2m/main.js',
        )
        css = {'screen': (
            settings.STATIC_URL + 'sortedm2m/widget.css',
        )}

    def render(self, name, value, attrs=None, choices=()):
        if value is None: value = []
        has_id = attrs and 'id' in attrs
        final_attrs = self.build_attrs(attrs, name=name)

        # Normalize to strings
        str_values = [force_text(v) for v in value]

        selected = []
        # Limit the images in the "order" container to the selected images (not all available images, which is
        # the default-queryset.
        selected_images = self.choices.queryset.filter(id__in=value)
        if selected_images.exists():
            for i, (option_value, option_label) in enumerate(selected_images.values_list('id', 'original_filename')):
                # If an ID attribute was given, add a numeric index as a suffix,
                # so that the checkboxes don't all have the same ID attribute.
                if has_id:
                    final_attrs = dict(final_attrs, id='%s_%s' % (attrs['id'], i))
                    label_for = ' for="%s"' % conditional_escape(final_attrs['id'])
                else:
                    label_for = ''

                cb = forms.CheckboxInput(final_attrs, check_test=lambda value: value in str_values)
                option_value = force_text(option_value)
                rendered_cb = cb.render(name, option_value)
                option_label = force_text(extended_filer_file_unicode(option_value))
                item = {'label_for': label_for, 'rendered_cb': rendered_cb, 'option_label': option_label, 'option_value': option_value}
                if option_value in str_values:
                    selected.append(item)

        # re-order `selected` array according str_values which is a set of `option_value`s in the order they should be shown on screen
        ordered = []
        for value in str_values:
            for select in selected:
                if value == select['option_value']:
                    ordered.append(select)
        selected = ordered

        html = render_to_string(
            'ni_imagegallery/sorted_checkbox_select_multiple_widget.html', {'selected': selected}
        )
        return mark_safe(html)
