.. :changelog:

Changelog
---------

0.x (YYYY-MM-DD)
^^^^^^^^^^^^^^^^

* Comment.

0.1.6 (2016-08-16)
^^^^^^^^^^^^^^^^^^

* Sort images in file tree by name, filename

0.1.5 (2016-07-22)
^^^^^^^^^^^^^^^^^^

* Improve file tree script to cope with french punctuation rules (Also see: AS-20)

0.1.4 (2015-08-31)
^^^^^^^^^^^^^^^^^^

* Performance improvements for large galleries.


0.1.3 (2015-07-29)
^^^^^^^^^^^^^^^^^^

* Bugfix, the 'name' must be set.

0.1.2 (2015-06-02)
^^^^^^^^^^^^^^^^^^

* Add setting to control model field.

0.1.1 (2015-02-20)
^^^^^^^^^^^^^^^^^^

* Added option to filter images in select container.
* Further modularized code base.

0.1 (2015-03-12)
^^^^^^^^^^^^^^^^

* Initial release.



